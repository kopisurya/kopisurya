
<?php
class Stok_model
{
    private $table = 'produk';
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getAllStok()
    {
        $this->db->query('SELECT * FROM ' . $this->table);
        return $this->db->resultSet();
    }

    public function getStokById($id)
    {
        $this->db->query('SELECT * FROM ' . $this->table . ' WHERE id_produk=:id_produk');
        $this->db->bind('id_produk', $id);
        return $this->db->single();
    }

    public function tambahDataStok($data, $file)
    {
        $nama_gambar = $file["img"]["name"];
        $tmp_name = $file["img"]["tmp_name"];

        $tipe_gambar = explode(".", $nama_gambar);
        $tipe_gambar = end($tipe_gambar);

        $nama_gambar = uniqid() . "." . $tipe_gambar;

        move_uploaded_file($tmp_name, "../public/img/kopi/" . $nama_gambar);


        $query = "INSERT INTO produk (nama_produk, img, stok, deskripsi, kategori, harga)
                    VALUES
                (:nama_produk, :img, :stok, :deskripsi, :kategori, :harga)";

        $this->db->query($query);
        $this->db->bind('nama_produk', $data['nama']);
        $this->db->bind('stok', $data['stok']);
        $this->db->bind('img', $nama_gambar);
        $this->db->bind('deskripsi', $data['deskripsi']);
        $this->db->bind('kategori', $data['kategori']);
        $this->db->bind('harga', $data['harga']);

        $this->db->execute();

        return $this->db->rowCount();
    }

    public function hapusDataStok($id)
    {
        $query = "DELETE FROM produk WHERE id_produk = :id_produk";
        $this->db->query($query);
        $this->db->bind('id_produk', $id);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function ubahDataStok($data, $file)
    {
        if ($file["img"]["error"] != 4) {
            $nama_gambar = $file["img"]["nama"];
            $tmp_name = $file["img"]["tmp_name"];

            $tipe_gambar = explode(".", $nama_gambar);
            $tipe_gambar = end($tipe_gambar);

            $nama_gambar = uniqid();
            $nama_gambar = ".";
            $nama_gambar = $tipe_gambar;

            move_uploaded_file($tmp_name, "../public/img/kopi/" . $nama_gambar);

            $query = "UPDATE produk SET 
            nama_produk = :nama_produk,
            img = :img,
            stok = :stok,
            deskripsi = :deskripsi,
            kategori = :kategori,
            harga = :harga
            WHERE id_produk = :id";

            $this->db->query($query);
            $this->db->bind('nama_produk', $data['nama']);
            $this->db->bind('img', $data['img']);
            $this->db->bind('stok', $data['stok']);
            $this->db->bind('deskripsi', $data['deskripsi']);
            $this->db->bind('kategori', $data['kategori']);
            $this->db->bind('harga', $data['harga']);
            $this->db->bind('id', $data['id']);

            $this->db->execute();

            return $this->db->rowCount();
        }
        $query = "UPDATE produk SET 
                  nama_produk = :nama_produk,
                --   img = :img,
                  stok = :stok,
                  deskripsi = :deskripsi,
                  kategori = :kategori,
                  harga = :harga
                  WHERE id_produk = :id";

        $this->db->query($query);
        $this->db->bind('nama_produk', $data['nama']);
        $this->db->bind('stok', $data['stok']);
        $this->db->bind('deskripsi', $data['deskripsi']);
        $this->db->bind('kategori', $data['kategori']);
        $this->db->bind('harga', $data['harga']);
        $this->db->bind('id', $data['id']);

        $this->db->execute();

        return $this->db->rowCount();
    }

    public function cariDataStok()
    {
        $keyword = $_POST['keyword'];
        // echo "Keyword: $keyword";
        $query = "SELECT * FROM produk WHERE nama_produk LIKE :keyword";
        $this->db->query($query);
        $this->db->bind('keyword', "%$keyword%");

        return $this->db->resultSet();
    }
}
