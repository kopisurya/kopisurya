<?php
class User_model
{
    // surya
    private $nama = "surya";
    //end

    private $table = 'user';
    private $db;

    public function getUser()
    {
        return $this->nama;
    }

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getAllUser()
    {
        $this->db->query('SELECT * FROM ' . $this->table);
        return $this->db->resultSet();
    }

    public function getUserById($id)
    {
        $this->db->query('SELECT id_user, nama, email FROM ' . $this->table . ' WHERE id_user=:id_user');
        $this->db->bind('id_user', $id);
        return $this->db->single();
    }

    public function tambahDataUser($data)
    {
        // var_dump($data);
        // die;

        $query = "INSERT INTO user (nama, email, password, role)
                    VALUES
                (:nama, :email, :password, :role)";

        $this->db->query($query);
        $this->db->bind('nama', $data['nama']);
        $this->db->bind('email', $data['email']);
        $this->db->bind('password', $data['password']);
        $this->db->bind('role', $data['level']);

        $this->db->execute();

        return $this->db->rowCount();
    }

    public function hapusDataUser($id)
    {
        $query = "DELETE FROM user WHERE id_user = :id_user";
        $this->db->query($query);
        $this->db->bind('id_user', $id);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function ubahDataUser($data)
    {
        // var_dump($data);
        $query = "UPDATE user SET 
                  nama = :nama,
                  email = :email,
                  password = :password,
                  role = :role
                  WHERE id_user = :id";

        $this->db->query($query);
        $this->db->bind('nama', $data['nama']);
        $this->db->bind('email', $data['email']);
        $this->db->bind('password', $data['password']);
        $this->db->bind('role', $data['level']);
        $this->db->bind('id', $data['id']);

        $this->db->execute();

        return $this->db->rowCount();
    }

    public function cariDataUser()
    {
        $keyword = $_POST['keyword'];
        // var_dump($keyword);
        // die;
        // echo "Keyword: $keyword";
        $query = "SELECT * FROM user WHERE nama LIKE :keyword";
        $this->db->query($query);
        $this->db->bind('keyword', "%$keyword%");

        return $this->db->resultSet();
    }
}
