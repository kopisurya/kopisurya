<?php

class Data_user
{
    private $nama = 'GungWima Ganteng';
    public function getUser()
    {
        return $this->nama;
    }
    private $db;

    public function __construct()
    {
        $db = new Database();
        $this->db = $db->connect();
    }
    public function registerUser($nama, $email, $password)
    {
        // Hash kata sandi sebelum menyimpannya ke database (praktik terbaik keamanan)
        $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
        // insert user ke database
        $sql = "INSERT INTO user (nama, email, password) VALUES (:nama,:email,:password)";

        $stmt = $this->db->prepare($sql);

        $stmt->bindParam(":nama", $nama);
        $stmt->bindParam(":email", $email);
        $stmt->bindParam(":password", $hashedPassword);

        return $stmt->execute();
    }
    public function checkLogin($email, $password)
    {
        $query = "SELECT * FROM user WHERE email = :email";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(":email", $email);
        $stmt->execute();
        $stmt->rowCount();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($user['id_user'] && password_verify($password, $user["password"])) {
            //jika email ditemukan, verifikasi kata sandi
            // return $user['id'];
            return $user;
        } else {
            // jika login gagal kembalikan false
            return false;
        }
    }
}
