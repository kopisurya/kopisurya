<div class="container mt-4">

    <div class="row">
        <div class="col-lg-6">
            <?php Flasher::flash(); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 mb-3">
            <form action="<?= BASEURL; ?>/stok/cari" method="post">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Cari produk..." name="keyword" id="keyword" autocomplete="off">
                    <button class="btn text-light" style="background-color: #E69F3E;" type="submit" id="cari">Search</button>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 mb-3">
            <button type="button" class="btn text-light tombolTambahData" style="background-color: #E69F3E;" id="formModalTambah" data-bs-toggle="modal" data-bs-target="#formModal">
                Tambah Produk
            </button>
        </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">Id Stok</th>
                <th scope="col">Nama</th>
                <th scope="col">Gambar</th>
                <th scope="col">Stok</th>
                <th scope="col">Deskripsi</th>
                <th scope="col">Kategori</th>
                <th scope="col">Harga</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($data['stok'] as $stok) : ?>
                <tr>
                    <th scope="row"><?= $stok['id_produk']; ?></th>
                    <td><?= $stok['nama_produk']; ?></td>
                    <td> <img src="<?= BASEURL . '/img/kopi/' . $stok['img']; ?>" width="100px"></td>
                    <td><a href="" class="badge bg-secondary"><?= $stok['stok'] ?></a></td>
                    <td><?= $stok['deskripsi']; ?></td>
                    <td><?= $stok['kategori']; ?></td>
                    <td><?= $stok['harga']; ?></td>

                    <td>
                        <a href=" <?= BASEURL; ?>/stok/hapus/<?= $stok['id_produk']; ?>" class="badge text-bg-danger" onclick="return confirm('Yakin Ingin Menghapus?');">delete</a>
                        <a href="<?= BASEURL; ?>/stock/update/<?= $stok['id_produk']; ?>" class="badge text-bg-success tampilModalUbah" id="formModalUbah" data-bs-toggle="modal" data-bs-target="#formModal" data-id="<?= $stok['id_produk']; ?>">update</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <!-- Modal -->
    <div class="modal fade" id="formModal" tabindex="-1" aria-labelledby="formModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="formModalLabel">Tambah Produk</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="<?= BASEURL; ?>/stok/tambah" method="post" id="formaction" enctype="multipart/form-data">
                        <input type="hidden" name="id" id="id">

                        <div class="form-group">
                            <label for="nama" class="form-label">Nama Produk</label>
                            <input type="text" class="form-control" id="nama" name="nama">
                        </div>

                        <div class="form-group">
                            <label for="img" class="form-label">Gambar</label><br>
                            <img src="" alt="" id="foto" width="100px">
                            <input type="file" class="form-control" id="img" name="img">
                        </div>

                        <div class="form-group">
                            <label for="stok" class="form-label">Stok</label><br>
                            <input type="number" class="form-control" id="stok" name="stok">
                        </div>

                        <div class="form-group">
                            <label for="desk" class="form-label">Deskripsi</label>
                            <input type="text" class="form-control" id="desk" name="deskripsi">
                        </div>

                        <div class="form-group">
                            <label for="kategori" class="form-label">Kategori Produk</label><br>
                            <select class="custom-select" id="kategori" name="kategori">
                                <option value="" disabled selected>Open this select menu</option>
                                <option value="robusta" name="kategori">Robusta</option>
                                <option value="arabika" name="kategori">Arabika</option>
                                <option value="toraja" name="kategori">Toraja</option>
                                <option value="java" name="kategori">Java</option>
                                <option value="balikintamani" name="kategori">Bali Kintamani</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="harga" class="form-label">Harga Produk</label>
                            <input type="text" class="form-control" id="harga" name="harga">
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-warning">Tambah Data</button>
                </div>
                </form>
            </div>
        </div>
    </div>