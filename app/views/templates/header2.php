<!DOCTYPE html>
<html lang="en">

<head>
    <title><?php echo $data["judul"]; ?></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?= BASEURL; ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= BASEURL; ?>/css/user.css">
    <link rel="stylesheet" href="<?= BASEURL ?>/fontawesome/css/all.css">
    <link rel="stylesheet" href="<?php echo BASEURL; ?>/css/style1.css">


</head>

<body>
    <!-- <nav class="navbar navbar-expand-lg navbar-light bg-black">
  <a class="navbar-brand bg-black font-weight-bold  " href="#">MVC</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link active" href="#">Home <span class="sr-only">(current)</span></a> 
      <a class="nav-item nav-link" href="/blog">Blog</a>
      <a class="nav-item nav-link" href="#">User</a>
      
    </div>
  </div>
</nav>
    
</div> -->
    <div class=" d-flex bg-white  ">

        <div class=" container col-2  d-block bg-surya">
            <!-- <div class="row flex-nowrap"> -->
            <div class=" bg-surya w-auto">
                <div class="d-flex flex-column align-items-center  text-white  ">
                    <div class=" align-items-center justify-content-center mt-4 ">
                        <img src="<?= BASEURL; ?>/img/produk/fotoku1.jpg" alt="" height="100px" width="100px" class="rounded-circle    ">

                    </div>
                    <div class=" align">

                        <div class="fw-bold text-center fs-6  ">Gede Surya Winata</div>
                    </div>

                    <ul class="nav nav-pills flex-column mb-sm-auto mt-3 align-items-center align-items-sm-start" id="menu">
                        <li class="nav-item mb-2">
                            <a href="<?= BASEURL; ?>/home" class="nav-link align-middle px-0 font">
                                <span class="ms-1 d-none d-sm-inline fw-bold text-white">Home</span>
                            </a>
                        </li>

                        <li class=" mb-2">
                            <a href="<?= BASEURL; ?>/about" class="nav-link px-0 align-middle">
                                <span class="ms-1 d-none d-sm-inline fw-bold text-white ">About</span></a>
                        </li>
                        <li class=" mb-2">
                            <a href="<?= BASEURL; ?>/produk" class="nav-link px-0 align-middle">
                                <span class="ms-1 d-none d-sm-inline fw-bold text-white">Products</span> </a>

                        </li>
                        <li class=" mb-2">
                            <a href="<?= BASEURL; ?>/contact" class="nav-link px-0 align-middle">
                                <span class="ms-1 d-none d-sm-inline fw-bold text-white">Contact</span> </a>
                        </li>
                        <li class=" mb-2">
                            <a href="<?= BASEURL; ?>/Auth" class="nav-link px-0 align-middle">
                                <span class="ms-1 d-none d-sm-inline fw-bold text-white">Login</span> </a>
                        </li>
                        <li class=" mb-2">
                            <a href="<?= BASEURL; ?>/Auth/tryLogout" class="nav-link px-0 align-middle">
                                <span class="ms-1 d-none d-sm-inline fw-bold text-white">Logout</span> </a>
                        </li>
                        <li class=" mb-2">
                            <a href="<?= BASEURL; ?>/Auth/cart" class="nav-link px-0 align-middle">
                                <span class="ms-1 d-none d-sm-inline fw-bold text-white">Cart</span> </a>
                        </li>
                    </ul>
                    <div class="mb-5">
                        <i class="fa-solid fa-arrow-right-from-bracket text-white fs-5 fa-rotate-180"></i>
                        <div><a href="<?= BASEURL; ?>/Auth/tryLogout">Exit</a>
                        </div>
                    </div>

                    <hr>
                </div>
            </div>

        </div>



</html>