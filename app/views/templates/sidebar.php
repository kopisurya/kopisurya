<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <title>Halaman <?= $data['title']; ?></title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= BASEURL; ?>/css/style.css" />
    <link href="<?= BASEURL; ?>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css" />
    <!-- <link rel="stylesheet" href="<?= BASEURL ?>/css/sb-admin-2.css" /> -->

</head>

<body>

    <div class="wrapper d-flex align-items-stretch">
        <ul class="navbar-nav sidebar text-white teaccordion" style="background-color: #E69F3E;" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= BASEURL; ?>">
                <div class="sidebar-brand-icon rotate-n-15" style="color: white;">
                    <img src="<?= BASEURL; ?>/img/coffe.png" alt="" width="35px">
                </div>
                <div class="sidebar-brand-text mx-3" style="color: white;">KOPI SURYA</div>
            </a>

            <hr class=" sidebar-divider my-0" />

            <!-- Nav Item - Dashboard -->
            <li class="nav-item <?= $data['a1']; ?>">
                <a class="nav-link" href="<?= BASEURL; ?>">
                    <i class="fas fa-fw fa-table"></i>
                    <span>Dashboard</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider" />

            <!-- Heading -->
            <div class="sidebar-heading">Interface</div>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <li class="nav-item <?= $data['a2']; ?>">
                <a class="nav-link collapsed" href="<?= BASEURL; ?>/stok/index" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Stok Produk</span>
                </a>
            </li>

            <!-- Nav Item - Utilities Collapse Menu -->
            <li class="nav-item <?= $data['a3']; ?>">
                <a class="nav-link collapsed" href="<?= BASEURL; ?>/user/index" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
                    <i class="fas fa-fw fa-wrench"></i>
                    <span>Users</span>
                </a>
            </li>
        </ul>
        <!-- Page Content  -->
        <div class="container-fluid">

            <!-- </div> -->
            <!-- </div> -->