<div class="container col-10">
    <div class=" justify-content-center align-items-center  ">
        <div class=" row justify-content-center align-align-items-center">
            <div class="align-content-center  justify-content-center kiri ">
                <h2 class="text-brown">About</h2>
            </div>
            <div class="d-flex mb-4 mt-5 justify-content-center">
                <img src="<?= BASEURL ?>/img/produk/kopisusu.png " height="200px" width="200px" alt="">
                <h3 class="px-3 w-75">Dengan pengolahan yang terstruktur kami
                    Menghadirkan kenikmatan kopi yang
                    pas dan cocok di minum oleh kalangan
                    Mana saja</h3>
            </div>
            <div class="d-flex mb-4 mt-5 justify-content-center">
                <img src="<?= BASEURL ?>/img/produk/kedai.jpg" height="200px" width="200px" alt="">
                <h3 class="px-3  w-75">Berdiri sejak Tahun 2023, yang pada awalnya
                    menjadi penikmat kopi kemudian bertemu inspirasi
                    dan membuat brand yang bernama "Kopi Surya".
                    "Kopi Surya" bisa menjadi sebutan untuk kopi
                    yang tumbuh dengan paparan sinar matahari
                    yang tinggi, memberikan cita rasa yang hangat,
                    penuh, dan berenergi.</h3>
            </div>

        </div>

    </div>


</div>