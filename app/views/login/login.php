<!DOCTYPE html>
<html lang="en">

<head>
    <title><?php echo $data["judul"]; ?></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?= BASEURL; ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= BASEURL; ?>/css/user.css">
    <link rel="stylesheet" href="<?= BASEURL ?>/fontawesome/css/all.css">
    <link rel="stylesheet" href="<?php echo BASEURL; ?>/css/style1.css">


</head>

<body>

    <div class="container d-flex justify-content-center Form-Value" style="margin-top: 70px;">
        <section>
            <form action="<?= BASEURL; ?>/auth/tryLogin" method="post">
                <div class="row justify-content-between signUpView">
                    <div class="col-4 box1">
                        <img class="" style="object-fit: cover; background-position: bottom !important;" width="500px" height="445px" src="<?php echo BASEURL; ?>/img/bg-kopi.png" alt="gambarKopi">
                    </div>

                    <div class="box-form-signup col-5 mx-2 mt-3 ">
                        <div class="box-side text-center mt-4 ">
                            <h1 class="mb-5">Sign In</h1>
                            <div class="row  pt-3">
                                <div class="col-4">
                                    <label for="inputUsername" class="col-form-label label1">Username</label>
                                </div>
                                <div class="col-7 mb-3 kolomInput">
                                    <input type="email" id="inputUsername" class="form-control" width="200" name="email" required>
                                </div>
                            </div>
                            <div class="row align-items-center mb-5">
                                <div class="col-4 ">
                                    <label for="inputPassword" class="col-form-label label1">Password</label>
                                </div>
                                <div class="col-7 kolomInput">
                                    <input type="password" id="inputPassword" class="form-control" name="password">
                                </div>
                                <div class="col-7 kolomInput">
                                    <input type="hidden" id="role" class="form-control" name="role">
                                </div>
                            </div>

                            <div class="formBottom px-4 ">
                                <div class="formBottomChild px-4">
                                    <button type="submit" name="submit" class="btn btnColor" style="width: 100%;">Sign In</button>
                                    <p>Dont Have an Account? <a href="<?php echo BASEURL; ?>/auth/register/">Register</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>