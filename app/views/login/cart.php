<div class="container">
    <div class="row mt-3 d-flex justify-content-between">
        <div class="row part-Shopping col-9 pt-3">
            <div class="col-10">
                <h4>Shopping Cart</h4>
            </div>
            <div class="col-2">
                <h4>3 Items</h4>
            </div>
            <hr>
        </div>
        <div class="row part-Payment col-3 pt-3">
            <div class="col-12 pb-2">
                <h4>Order Summary</h4>
                <hr>
            </div>

        </div>

        <!-- Part Item -->
        <div class="row part-Shopping col-9">
            <div class="partItem1 row col-12">
                <div class="row col-8">
                    <div class="col-6">
                        <h6>Product Details</h6>
                    </div>
                </div>
                <div class="row col-4">
                    <div class="col-4">
                        <h6>Quantity</h6>
                    </div>
                    <div class="col-4">
                        <h6>Price</h6>
                    </div>
                    <div class="col-4">
                        <h6>Total</h6>
                    </div>
                </div>
            </div>
            <div class="partItem2 row col-12 mt-4">
                <div class="row col-8">
                    <div class="col-6 d-flex align-items-center">
                        <img src="<?php echo BASEURL; ?>/img/bg-kopi.png" width="70px" height="60px" alt="">
                        <h6 class="mx-3">Coffe 1</h6>
                    </div>
                </div>
                <div class="row col-4">
                    <div class="col-3 mx-2">
                        <input class="" type="number" style="width: 40px;">
                    </div>
                    <div class="col-4">
                        $45.000
                    </div>
                    <div class="col-3">
                        $45.000
                    </div>
                </div>
            </div>
            <div class="partItem2 row col-12 mt-4">
                <div class="row col-8">
                    <div class="col-6 d-flex align-items-center">
                        <img src="<?php echo BASEURL; ?>/img/bg-kopi.png" width="70px" height="60px" alt="">
                        <h6 class="mx-3">Coffe 2</h6>
                    </div>
                </div>
                <div class="row col-4">
                    <div class="col-3 mx-2">
                        <input class="" type="number" style="width: 40px;">
                    </div>
                    <div class="col-4">
                        $45.000
                    </div>
                    <div class="col-3">
                        $45.000
                    </div>
                </div>
            </div>
            <div class="partItem2 row col-12 mt-4">
                <div class="row col-8">
                    <div class="col-6 d-flex align-items-center">
                        <img src="<?php echo BASEURL; ?>/img/bg-kopi.png" width="70px" height="60px" alt="">
                        <h6 class="mx-3">Coffe 3</h6>
                    </div>
                </div>
                <div class="row col-4">
                    <div class="col-3 mx-2">
                        <input class="" type="number" style="width: 40px;">
                    </div>
                    <div class="col-4">
                        $45.000
                    </div>
                    <div class="col-3">
                        $45.000
                    </div>
                </div>
            </div>
            <div class="partItem2 row col-12 mt-4">
                <div class="row col-8">
                    <div class="col-6 d-flex align-items-center">
                        <img src="<?php echo BASEURL; ?>/img/bg-kopi.png" width="70px" height="60px" alt="">
                        <h6 class="mx-3">Coffe 4</h6>
                    </div>
                </div>
                <div class="row col-4">
                    <div class="col-3 mx-2">
                        <input class="" type="number" style="width: 40px;">
                    </div>
                    <div class="col-4">
                        $45.000
                    </div>
                    <div class="col-3">
                        $45.000
                    </div>
                </div>
            </div>
            <div class="partItem2 row col-12 mt-4">
                <div class="row col-8">
                    <div class="col-6 d-flex align-items-center">
                        <a href="<?php echo BASEURL; ?>/home">
                            <h6 class="mx-3"><- continue shopping</h6>
                        </a>
                    </div>
                </div>

            </div>


        </div>
        <!-- Orderan -->
        <div class="row part-Payment col-3">
            <form action="">
                <div class="row d-flex justify-content-between">
                    <div class="col-6">
                        Item 3
                    </div>
                    <div class="col-6">
                        $45.000
                    </div>
                    <div class="row mt-4">
                        <div class="col-6 ">
                            Shipping
                            <br>
                            <input class="mt-2" type="text" placeholder="Standar Delivery - $5000">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 mt-3">
                            Promo Code
                            </br />
                            <input class="mt-2" type="text" placeholder="Enter Your Code">
                        </div>
                    </div>
                    <div class="buttonPayment mt-4">
                        <button class="btn btn-primary">Apply</button>
                    </div>

                    <div class="row d-flex justify-content-between mt-4">
                        <div class="col-6">
                            Total Cost
                        </div>
                        <div class="col-6">
                            $45.000
                        </div>
                    </div>
                    <div class="buttonCheckout mt-4">
                        <button style="width: 180px;" class="btn btn-primary">Checkout</button>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>
</div>