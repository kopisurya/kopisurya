<div class="container mt-4">

    <div class="row">
        <div class="col-lg-6">
            <?php Flasher::flash(); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 mb-3">
            <form action="<?= BASEURL; ?>/user/cari" method="post">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Cari user..." name="keyword" id="keyword" autocomplete="off">
                    <button class="btn text-light" style="background-color: #E69F3E;" type="submit" id="cari">Search</button>
                </div>
            </form>
        </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">Id User</th>
                <th scope="col">Nama</th>
                <th scope="col">Email</th>
                <!-- <th scope="col">Password</th> -->
                <!-- <th scope="col">Role</th> -->
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($data['user'] as $user) : ?>
                <tr>
                    <th scope="row"><?= $user['id_user']; ?></th>
                    <td><?= $user['nama']; ?></td>
                    <td><?= $user['email']; ?></td>
                    <!-- <td><?= $user['password']; ?></td> -->
                    <!-- <td><?= $user['role']; ?></td> -->
                    <td>
                        <a href="<?= BASEURL; ?>/user/hapus/<?= $user['id_user'] ?>" class="badge text-bg-danger" onclick="return confirm('Yakin Ingin Menghapus?');">hapus</a>
                        <!-- <a href="<?= BASEURL; ?>/user/update/<?= $user['id_user']; ?>" class="badge text-bg-success tampilModalUbah2" id="formModalUbah" data-bs-toggle="modal" data-bs-target="#formModal" data-id="<?= $user['id_user']; ?>">update</a> -->
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<!-- Modal -->
<div class="modal fade" id="formModal" tabindex="-1" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="formModalLabel">Tambah Data User</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="<?= BASEURL; ?>/user/tambah" method="post" id="formaction">
                    <input type="hidden" name="id" id="id">

                    <div class="form-group">
                        <label for="nama" class="form-label">Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama" required>
                    </div>

                    <div class="form-group">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" class="form-control" id="email" name="email" required>
                    </div>
                    <!-- <div class="form-group">
                        <label for="text" class="form-label">Password</label>
                        <input type="text" class="form-control" id="password" name="password" required>
                    </div> -->
                    <!-- <div class="form-group">
                        <label for="level" class="form-label">Role</label>
                        <select class="custom-select" id="level" name="level" required>
                            <option value="" disabled selected>Open this select menu</option>
                            <option value="admin" name="role">admin</option>
                            <option value="user" name="role">user</option>
                        </select>
                    </div> -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-warning">Tambah Data</button>
            </div>
            </form>
        </div>
    </div>
</div>