<div class="container col-10">
    <div class=" justify-content-center align-items-center text-center ">
        <div class=" row justify-content-center align-align-items-center d-flex mt-4">
            <div class="align-content-center justify-content-center ">
                <h2 class="text-brown">Produk</h2>
            </div>
            <div class="text-end mb-5 ">
                <a class="text-black" href="<?= BASEURL ?>/produk/keranjang">
                    <i class="fa-solid fa-cart-shopping fs-keranjang "></i>
                </a>

            </div>
        </div>
    </div>
    <div>

        <!-- <div class="dropdown ">
            <a class="btn bg-surya dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Kategori
            </a>

            <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="#">Action</a></li>
                <li><a class="dropdown-item" href="#">Another action</a></li>
                <li><a class="dropdown-item" href="#">Something else here</a></li>
            </ul>
        </div>
    </div> -->

        <div class=" container d-flex pt-5">

            <div class="dropdown col-4 ">
                <a class="btn bg-surya fs-3 fw-bold  " href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Kategori
                    <i class="fa-solid fa-circle-chevron-down text-black "></i>
                </a>


                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="#">Kopi Saset</a></li>
                    <li><a class="dropdown-item" href="#">Kopi Bubuk</a></li>
                    <li><a class="dropdown-item" href="#">Kopi Siapsaji</a></li>
                </ul>
            </div>
            <form class="col-8 " action="<?= BASEURL  ?>/Produk/cari" method="post">

                <div class="d-flex align-items-center text-center  searching-100 form-control light-table-filter w-50 h-75 border-3 border-black rounded-pill ">
                    <button class="border-0 bg-transparent " type="submit" name="submit">
                        <i typ class="fa-solid fa-magnifying-glass"></i>
                    </button>

                    <input id="myInput" name="keyword" type="text" class="border-0 w-" data-table="table-hover" placeholder="Search Keyword...." autocomplete="off" />
                </div>

            </form>
        </div>


        <!-- Produk -->
        <div class="d-flex " data-bs-target="#exampleModal" data-bs-toggle="modal">
            <?php foreach ($data["produk"] as $p) : ?>
                <div class="container d-flex pt-5 pb-5" data-bs-toggle="modal" data-bs-target="#exampleModal">
                    <div class="col-4">
                        <div class="card card-produk">
                            <img src="<?= BASEURL . '/img/kopi/' . $p['img']; ?>" width="100px">
                            <div class="card-body">
                                <h5 class="card-title text-lg-center"><?= $p["nama_produk"]; ?></h5>
                                <p class="card-text">Rp.<?= $p["harga"]; ?></p>
                                <p class="card-text">Stok.<?= $p["stok"]; ?> </p>
                                <div class="d-flex justify-content-center pb-3">

                                    <button type="button" data-bs-toggle="modal" data-bs-target="#exampleModal" class="btn bg-surya rounded-pill w-50 fw-bold ">Beli</button>
                                    <i class="fa-solid fa-cart-plus fs-4 text-center pl-kereanjang  "></i>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="">
                <div class="container d-flex pt-5 pb-5">
                    <div class="col-4">
                        <div class="card card-produk2">
                            <div class="modal-header">

                                <button type="button" class="btn-close " data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>


                            <img src="<?= BASEURL ?>/img/kopi/<?= $p["img"]; ?>" class="card-img-top " height="100" width="100" alt="...">
                            <div class="card-body">
                                <h5 class="card-title text-lg-center"><?= $p["nama_produk"]; ?></h5>
                                <p class="card-text">Rp.<?= $p["harga"]; ?></p>
                                <p class="card-text">Stok.<?= $p["harga"]; ?> </p>
                                <div class="d-flex justify-content-center pb-3">

                                    <button type="button" data-bs-toggle="modal" data-bs-target="#exampleModal" class="btn bg-surya rounded-pill w-50 fw-bold ">Beli</button>
                                    <i class="fa-solid fa-cart-plus fs-4 text-center pl-kereanjang "></i>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>