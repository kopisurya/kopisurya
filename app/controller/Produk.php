<?php

class Produk extends Controller
{
    public function index()
    {
        $data["judul"] = "produk";
        $data["produk"] = $this->model("Stok_model")->getAllStok();
        $this->view("templates/header2", $data);
        $this->view("produk/index", $data);
        $this->view("templates/footer2");
    }

    public function keranjang()
    {
        $data["judul"] = "produk";
        $data["produk"] = $this->model("Stok_model")->getAllStok();
        $this->view("templates/header2", $data);
        $this->view("produk/keranjang", $data);
        $this->view("templates/footer2");
    }

    public function cari()
    {
        $data["judul"] = "produk";
        $data["produk"] = $this->model("Stok_model")->cariDataStok();
        $this->view("templates/header2", $data);
        $this->view("produk/index", $data);
        $this->view("templates/footer2");
    }
}
