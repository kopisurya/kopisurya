<?php
class Admin extends Controller
{
    public function __construct()
    {
        // session_start();
        // $_SESSION['role'] = $this->model('Data_user');   
        if ($_SESSION['user_role'] == 'user') {
            header('Location: ' . BASEURL . '/home');
        }
    }
    public function index()
    {
        $data = [
            "title" => "Admin",
        ];
        $data['a1'] = 'active';
        $data['a2'] = '';
        $data['a3'] = '';

        // $data['name'] = $this->model('User_model')->getUser();
        $this->view('templates/sidebar', $data);
        $this->view('admin/index', $data);
        $this->view('templates/endsidebar');
    }
}
