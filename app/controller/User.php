<?php

class User extends Controller
{
    public function index()
    {
        $data["title"] = 'user';
        // $data["stok-active"] = true;
        $data["user"] = $this->model('User_model')->getAllUser();
        $data['a1'] = '';
        $data['a2'] = '';
        $data['a3'] = 'active';
        $this->view('templates/sidebar', $data);
        $this->view('user/index', $data);
        $this->view('templates/endsidebar');
    }

    public function hapus($id)
    {
        header('Location: ' . BASEURL . '/user');
        if ($this->model('User_model')->hapusDataUser($id) > 0) {
            Flasher::setFlash('berhasil', 'dihapus', 'success');
        } else {
            Flasher::setFlash('gagal', 'dihapus', 'danger');
        }
    }

    public function cari()
    {
        $data["title"] = 'user';
        // $data["stok-active"] = true;
        $data["user"] = $this->model('User_model')->cariDataUser();
        $data['a1'] = '';
        $data['a2'] = 'active';
        $data['a3'] = '';
        $this->view('templates/sidebar', $data);
        $this->view('user/index', $data);
        $this->view('templates/endsidebar');
    }
}
