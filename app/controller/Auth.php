<?php

class Auth extends Controller
{

    private $userModel;

    // public function __construct($userModel){
    //       $this->userModel =  $userModel;
    // }

    public function __construct()
    {
        // session_start();

        if (isset($_SESSION["user_id"])) {
            header("Location: " . BASEURL . "/admin");
        }
    }
    public function index()
    {
        // if (isset($_SESSION["nama"])){
        //       header("location: home");
        // }
        $data['judul'] = 'login';
        $data['nama'] = $this->model('Data_user')->getUser();
        // $this->view('templates/header2', $data);
        $this->view('login/login', $data);
        $this->view('templates/footer');
    }

    public function register()
    {
        $data['judul'] = 'register';
        $data['nama'] = $this->model('Data_user')->getUser();
        // $this->view('templates/header2', $data);
        $this->view('login/register', $data);
        $this->view('templates/footer');
    }

    public function tryRegister()
    {
        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            //ambil data dari form pendaftaran(register)
            $email = $_POST['email'];
            $nama = $_POST['nama'];
            $password = $_POST['password'];

            //panggil model untuk mendaftarkan pengguna
            $result = $this->model('Data_user')->registerUser($nama, $email, $password);

            //validasi 
            if ($result == true) {
                header('Location:' . BASEURL . '/auth/login');
            } else {
                header('Location:' . BASEURL . '/auth/register');
                echo "gagal daftarkan pengguna </br>";
                return false;
            }
        }
    }
    public function tryLogin()
    {

        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            // mengambil data yang dikirimkan melalui metode POST (formulir login)
            $email = $_POST['email'];
            $password = $_POST['password'];

            //cek melalui model
            $user_id = $this->model('Data_user')->checkLogin($email, $password);
            if ($user_id !== false) {
                // Memulai session
                session_start();
                $_SESSION['user_id'] = $user_id['id_user'];
                $_SESSION['user_role'] = $user_id['role'];
                // var_dump($user_id);
                // die;
                // header('Location:' . BASEURL . 'about/index');
                if ($user_id['role'] == 'admin') {
                    header('Location: ' . BASEURL . '/admin');
                } else {
                    header('Location: ' . BASEURL . '/Home');
                }
                exit;
            } else {
                echo "tidak berhasil";
                return false;
            }
        }
    }
    public function trySession()
    {
        if (isset($_SESSION["user_id"])) {
            header("Location: " . BASEURL . "/admin/index");
            return true;
        }
    }
    public function tryLogout()
    {
        // session_start();
        session_unset();
        session_destroy();
        header('Location: ' . BASEURL . '/Auth/login');
    }
    public function cart()
    {
        $data['judul'] = 'Cart';
        $this->view('templates/header2', $data);
        $this->view('produk/keranjang');
        $this->view('templates/footer2');
    }
    // public function login(){
    //       $username = $_POST['username'];
    //       $password = $_POST['password'];
    //       $data['login'] = $this->model('Log_model')->getUser($username,$password);

    //       if($data['login'] == NULL){
    //             header("Location:".BASEURL."404");
    //       }else{
    //             foreach($data['login'] as $row):
    //                   $_SESSION['nama'] = $row['nama'];
    //                   header("Location:". BASEURL);
    //             endforeach;
    //       }
    // }


}
