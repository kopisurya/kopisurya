<?php
class Stok extends Controller
{
    public function index()
    {
        $data["title"] = 'stok';
        $data["stok-active"] = true;
        $data["stok"] = $this->model('Stok_model')->getAllStok();
        $data['a1'] = '';
        $data['a2'] = 'active';
        $data['a3'] = '';
        $this->view('templates/sidebar', $data);
        $this->view('stok/index', $data);
        $this->view('templates/endsidebar');
    }

    //Fungsi Crud Stok

    public function detail($id)
    {
        $data['title'] = 'Detail Stok';
        $data['stok'] = $this->model('Stok_model')->getStokById($id);
        $data['a1'] = '';
        $data['a2'] = 'active';
        $data['a3'] = '';
        $this->view('templates/sidebar', $data);
        $this->view('stok/detail', $data);
        $this->view('templates/endsidebar');
    }

    public function tambah()
    {
        if ($this->model('Stok_model')->tambahDataStok($_POST, $_FILES) > 0) {
            Flasher::setFlash('berhasil', 'ditambahkan', 'success');
        } else {
            Flasher::setFlash('gagal', 'ditambahkan', 'danger');
        }
        header('Location: ' . BASEURL . '/stok');
    }

    public function hapus($id)
    {
        header('Location: ' . BASEURL . '/stok');
        if ($this->model('Stok_model')->hapusDataStok($id) > 0) {
            Flasher::setFlash('berhasil', 'dihapus', 'success');
        } else {
            Flasher::setFlash('gagal', 'dihapus', 'danger');
        }
    }

    public function getubah()
    {
        echo json_encode($this->model('Stok_model')->getStokById($_POST['id']));
    }

    public function ubah()
    {
        if ($this->model('Stok_model')->ubahDataStok($_POST, $_FILES) > 0) {
            Flasher::setFlash('berhasil', 'diubah', 'success');
        } else {
            Flasher::setFlash('gagal', 'diubah', 'danger');
        }
        header('Location: ' . BASEURL . '/stok');
    }

    public function cari()
    {
        $data["title"] = 'stok';
        $data["stok-active"] = true;
        $data["stok"] = $this->model('Stok_model')->cariDataStok();
        $data['a1'] = '';
        $data['a2'] = 'active';
        $data['a3'] = '';
        $this->view('templates/sidebar', $data);
        $this->view('stok/index', $data);
        $this->view('templates/endsidebar');
    }
}
