<?php

class Home extends Controller
{
    public function index()
    {
        //echo "Home/index";
        $data["judul"] = "Home";


        $this->view("templates/header2", $data);
        $this->view("home/index", $data);
        $this->view("templates/footer2");
    }
}
