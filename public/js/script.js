$ (function() {
    
    $('.tombolTambahdata').on('Click', function(){
        $('#formModalLabel').html('Tambah Data Produk');
        $('.modal-footer button[type=submit]').html('Tambah Data');
    })

    // $('.tampilModalUbah').on('click', function(){
    //    $('#formModalLabel').html('Ubah Data Produk');
    //    $('.modal-footer button[type=submit]').html('Ubah Data');
    //    $('#formaction').attr('action', 'http://localhost/kopisurya/public/stok/ubah/' + $('#id').val(data.id));

    $(document).on('click', '.tampilModalUbah', function() {
        const id_produk = $(this).data('id'); // Mengambil nilai 'id' dari elemen yang di-klik
        $('#formModalLabel').html('Ubah Data Produk');
           $('.modal-footer button[type=submit]').html('Ubah Data');
           $('#formaction').attr('action', 'http://localhost/kopisurya/public/stok/ubah/' + id_produk);

        $.ajax({
            url: 'http://localhost/kopisurya/public/stok/getubah',
            data: {id : id_produk},
            method: 'post',
            dataType: 'json',
            success: function(data){
                // console.log(data);
                let src="http://localhost/kopisurya/public/img/kopi/" + data.img
                $('#nama').val(data.nama_produk);
                $('#stok').val(data.stok);
                $('#desk').val(data.deskripsi);
                $('#kategori').val(data.kategori);
                $('#harga').val(data.harga);
                $('#id').val(id_produk);
                $('#foto').attr('src', src);
            }
        })
    });
    
    // $(document).on('click', '.tampilModalUbah2', function(){
    //     const id_user = $(this).data('id'); // Mengambil nilai 'id' dari elemen yang di-klik
    //     $('#formModalLabel').html('Ubah Data Produk');
    //        $('.modal-footer button[type=submit]').html('Ubah Data');
    //        $('#formaction').attr('action', 'http://localhost/kopisurya/public/user/ubah/' + id_user);

    //     $.ajax({
    //         url: 'http://localhost/kopisurya/public/user/getubah',
    //         data: {id : id_user},
    //         method: 'post',
    //         dataType: 'json',
    //         success: function(data){
    //             // console.log(data);
    //             $('#nama').val(data.nama);
    //             $('#email').val(data.email);
    //             $('#password').val(data.password);
    //             $('#level').val(data.role);
    //             $('#id').val(id_user);
    //         }
    //     })
    // });
});